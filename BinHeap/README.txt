This is a simple binary heap in Objective C. It supports the basics:
Add an item, pop the root element, create from an array, and not much
else.

Clients must provide a delegate that returns the priority of
objects. This is a min priority heap, meaning objects with smaller
priorities are popped off first.

Interally, the heap uses an NSMutableArray for storage. This means
that it doesn't have a size limit but it also inherits any performance
issues NSMutableArray may have.

Known issues:

Union is currently not supported. This would be easy to add but I have
no need for it right now. Likewise, bulk add is not implement as that
is essentially be the same operation.

Using a delegate to fetch an item's weight is only one of many ways to
skin this cat. It would probably be better to use an Objective-C block
as clients won't find themselves creating new subclasses for such a
minor behavior. However, having a delegate allows for future behavior
extensions beyond simple comparisons.

Using a delegate to fetch a priority instead of using a comparator is
probably wrong. Using a comparator would allow the heap to be neither
min nor max, it would just be whatever the comparator prefers.

Sort is not implemented. You already have plenty of sort options
available to you as a developer so it isn't clear if there's any
reason to add a sort behavior.

The heap does not forbid addition of identical items. This is mostly
harmless but would be an issue if it supported set operations.

License:

I'm using the FreeBSD license. This lets you do almost anything.

Copyright (c) 2012, Charlie Reiman
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met: 

1. Redistributions of source code must retain the above copyright notice, this
   list of conditions and the following disclaimer. 
2. Redistributions in binary form must reproduce the above copyright notice,
   this list of conditions and the following disclaimer in the documentation
   and/or other materials provided with the distribution. 

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those
of the authors and should not be interpreted as representing official policies, 
either expressed or implied, of the FreeBSD Project.
