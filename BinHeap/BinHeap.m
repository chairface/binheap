//
//  BinHeap.m
//
//  Created by Charlie Reiman on 6/21/12.
//
// Copyright (c) 2012, Charlie Reiman
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met: 
// 
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies, 
// either expressed or implied, of the FreeBSD Project.

#import "BinHeap.h"

@interface BinHeap ()

- (void) heapifyUpFrom:(NSInteger)atIndex;
- (void) heapifyDownFrom:(NSInteger)atIndex;
- (NSInteger) interiorNodeCount;
- (NSInteger) leafNodeCount;

@end

// Simple static functions to return the child or parent
// indicies. 
inline static NSInteger parentNode(NSInteger node)
{
    return (node-1) >> 1;
}

inline static NSInteger leftChildNode(NSInteger node)
{
    return (node<<1) + 1;
}

inline static NSInteger rightChildNode(NSInteger node) 
{
    return (node<<1) + 2;
}

@implementation BinHeap

@synthesize itemArray = _itemArray;
@synthesize delegate = _delegate;


#pragma mark - External methods

- (BinHeap*) initWithDelegate:(id <BinHeapDelegate>)delegate
{
    if ((self=[super init]) != nil) {
        _itemArray = [[NSMutableArray alloc] initWithCapacity:16];
        _delegate = delegate;
    }
    return self;
}

- (BinHeap*) initWithDelegate:(id <BinHeapDelegate>)delegate contents:(NSArray*)array
{
    if ((self=[super init]) != nil) {
        _itemArray = [[NSMutableArray alloc] initWithArray:array];
        _delegate = delegate;
        int nodeIndex = [self interiorNodeCount];
        while (nodeIndex >=0) {
            [self heapifyDownFrom:nodeIndex];
            nodeIndex -=1 ;
        }
    }
    return self;
}

- (NSInteger) count
{
    return [self.itemArray count];
}

- (BOOL) empty
{
    return [self.itemArray count] == 0;
}

- (void) addItem:(id)item
{
    [self.itemArray addObject:item];
    [self heapifyUpFrom:[self.itemArray count]-1];
}

- (id) popMin
{
    if ([self empty]) {
        return nil;
    }
    id minItem = [self.itemArray objectAtIndex:0];
    [self.itemArray replaceObjectAtIndex:0 withObject:[self.itemArray lastObject]];
    [self.itemArray removeLastObject];
    if (![self empty]) {
        [self heapifyDownFrom:0];
    }
    return minItem;
}

- (id) peekMin
{
    if ([self empty]) {
        return nil;
    }
    return [self.itemArray objectAtIndex:0];
}

#pragma mark - Internal methods

- (NSInteger) interiorNodeCount
{
    return [self.itemArray count]/2;
}

- (NSInteger) leafNodeCount
{
    return ([self.itemArray count]+1)/2;
}

// This fixes up the heap after appending a new item.
- (void) heapifyUpFrom:(NSInteger)atIndex
{
    while (atIndex > 0) {
        id thisItem = [self.itemArray objectAtIndex:atIndex];
        NSInteger parentIndex = parentNode(atIndex);
        id parentItem = [self.itemArray objectAtIndex:parentIndex];
        
        if ([self.delegate priorityInHeap:self forObject:thisItem] <
            [self.delegate priorityInHeap:self forObject:parentItem]) {
            [self.itemArray exchangeObjectAtIndex:atIndex withObjectAtIndex:parentIndex];
            atIndex = parentIndex;
        }
        else {
            break;
        }
    }
    
}

// This is called siftDown in other implementations. It fixes up the heap
// after popping the minimum item.
- (void) heapifyDownFrom:(NSInteger)atIndex
{
    while (atIndex < [self interiorNodeCount]) {
        NSInteger atPriority = [self.delegate priorityInHeap:self forObject: [self.itemArray objectAtIndex:atIndex]];
        NSInteger minIndex = atIndex;
        NSInteger leftIndex = leftChildNode(atIndex);
        NSInteger rightIndex = rightChildNode(atIndex);
        
        NSInteger testPriority;
        
        if (leftIndex < [self.itemArray count]) {
            testPriority = [self.delegate priorityInHeap:self forObject: [self.itemArray objectAtIndex:leftIndex]];
            if (testPriority < atPriority) {
                minIndex = leftIndex;
                atPriority = testPriority;
            }
        }
        if (rightIndex < [self.itemArray count]) {
            testPriority = [self.delegate priorityInHeap:self forObject: [self.itemArray objectAtIndex:rightIndex]];
            if (testPriority < atPriority) {
                minIndex = rightIndex;
                // atPriority == doesn't matter at this point.
            }
        }
        if (minIndex != atIndex) {
            [self.itemArray exchangeObjectAtIndex:atIndex withObjectAtIndex:minIndex];
            // Set up for iteration.
            atIndex = minIndex;
        }
        else {
            break;
        }
    }
}

@end

