//
//  BinHeapTests.m
//  BinHeapTests
//
//  Created by Charlie Reiman on 6/21/12.
//
// Copyright (c) 2012, Charlie Reiman
// All rights reserved.
// 
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions are met: 
// 
// 1. Redistributions of source code must retain the above copyright notice, this
// list of conditions and the following disclaimer. 
// 2. Redistributions in binary form must reproduce the above copyright notice,
// this list of conditions and the following disclaimer in the documentation
// and/or other materials provided with the distribution. 
// 
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
// ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
// WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
// DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
// ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
// (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
//  LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
// 
// The views and conclusions contained in the software and documentation are those
// of the authors and should not be interpreted as representing official policies, 
// either expressed or implied, of the FreeBSD Project.


#import "BinHeap.h"
#import "BinHeapTests.h"

// Just a thing to store in the heap
@interface Thing  : NSObject
@property NSInteger priority;
@end

@implementation Thing
@synthesize priority = _priority;

- (Thing*) initWithPriority:(NSInteger)p
{
    if ((self=[super init])!=nil) {
        _priority = p;
    }
    return self;
}

@end

@implementation BinHeapTests

- (void) setUp
{
    [super setUp];
    
    // Set-up code here.
}

- (void) tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (NSInteger) priorityInHeap:(BinHeap *)heap forObject:(Thing*)obj
{
    return [obj priority];
}

- (void) testExample
{
    
    BinHeap *testHeap1 = [[BinHeap alloc] initWithDelegate:self];
    int bigger, smaller;
    
    // Insert nodes in increasing order
    int i;
    for (i = 1; i <= 50; i++) {
        [testHeap1 addItem:[[Thing alloc] initWithPriority:i]];
    }
    
    smaller = [[testHeap1 popMin] priority];    
    while (![testHeap1 empty]) {
        bigger = [[testHeap1 popMin] priority];
        if (bigger < smaller) {
            STFail(@"Heap ordering failed in forward storage case");
        }
    }

    // Insert nodes in decreasing order
    for (i=50; i >= 1; i--) {
        [testHeap1 addItem:[[Thing alloc] initWithPriority:i]];
    }
    
    smaller = [[testHeap1 popMin] priority];    
    while (![testHeap1 empty]) {
        bigger = [[testHeap1 popMin] priority];
        if (bigger < smaller) {
            STFail(@"Heap ordering failed in reverse storage case");
        }
    }
    
    // Test pop after empty
    id shouldBeNil = [testHeap1 popMin];
    if (shouldBeNil != nil) {
        STFail(@"nil pop failed.");
    }
    
    // One item test
    [testHeap1 addItem:[[Thing alloc] initWithPriority: 5]];
    if ([[testHeap1 popMin] priority] != 5) {
        STFail(@"Got wrong priority in single item heap");
    }
    
    // Random array test. Also tests array initializer
    const int arraySize = 200;
    NSMutableArray *randomArray = [[NSMutableArray alloc] initWithCapacity:arraySize];
    for (i=0; i < arraySize; i++) {
        [randomArray addObject:[[Thing alloc] initWithPriority:random()]];
    }
    testHeap1 = [[BinHeap alloc] initWithDelegate: self contents: randomArray];
    
    smaller = [[testHeap1 popMin] priority];    
    while (![testHeap1 empty]) {
        bigger = [[testHeap1 popMin] priority];
        if (bigger < smaller) {
            STFail(@"Heap ordering failed in random array case");
        }
    }
}

@end
